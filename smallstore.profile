<?php

require_once('profiles/default/default.profile');





/**
 * Implementation of hook_profile_details().
 */
function smallstore_profile_details() {
  return array(
    'name' => 'Small Store',
    'description' => 'Select this profile to install the Small Store version of Drupal. Small Store is a basic e-commerce site.',
  );
}





/**
 * Implementation of hook_profile_modules().
 */
function smallstore_profile_modules() {
	return array(

		// Drupal modules enabled by default
		'color', 'comment', 'help', 'menu', 'taxonomy', 'dblog',

		// First, of course
		'admin_menu', 'adminrole',

		// CCK
		'content', 'content_copy', 'fieldgroup', 'number', 'text',
		'optionwidgets', 'userreference', 'filefield', 'filefield_token', 'nodereference',

			// Must turn on imageapi in order to turn on imagefield
			'imageapi', 'imagefield',

		// Views
		'views', 'views_ui', 'views_bulk_operations', 'views_export', 'views_slideshow',

		// Core optional
		'aggregator', 'blog', 'contact', 'path', 'ping', 'profile', 'search', 'statistics', 'syslog', 'trigger', 'upload',

		// Date/time
		'date_api', 'date_timezone', 'calendar', 'jcalendar', 'date', 'date_copy', 'date_popup', 'date_repeat', 'calendar_ical',

		// Image API and Image Cache
		'imageapi_gd', 'imagecache', 'imagecache_ui',

		// Other
		'advanced_help', 'nice_menus', 'token', 'token_actions', 

		// Printer, email, and PDF
		'print', 'print_mail', 'print_pdf',

		// WYSIWYG
		'ckeditor', 'imce',
	);
}





/**
 * Implementation of hook_profile_task().
 */
function smallstore_profile_task(&$task, $url) {
	default_profile_tasks();
}

?>
