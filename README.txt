
This install profile creates a website with a small store.

Based on Ubercart ecommerce. Defaults to PayPal.

***********************************************************
Installation instructions
***********************************************************

- Place the smallstore install profile in /profiles.

  /profiles/smallstore/smallstore.install
  /profiles/smallstore/README.txt

- Run the install script and choose the SmallStore profile.
